let http = require ('http')

let courses = [
    {
        "name": "BS Nursing",
        "building": 3
    },
    {
        "name": "BS Criminology",
        "building": 5
    }
]

let port = 4000

let server = http.createServer(function (req,res){
    if (req.url == '/' && req.method == 'GET'){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end('Welcome to booking system')
    } else if (req.url == '/profile' && req.method == 'GET'){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end('Welcome to your profile')
    } else if (req.url == '/courses'&& req.method == 'GET'){
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(courses))
        res.end("Here's our courses available")
    } else if (req.url == '/addCourse' && req.method == 'POST'){

        let request_body = ' '

        req.on('data', function(data){
            request_body += data
            console.log(request_body)
        })
        req.on('end', function(){
            console.log(typeof request_body)
        })
        request_body = JSON.parse(request_body)

        let new_course = {
            "name": request_body.name,
            "building": request_body.building,
        }

            users.push(new_course)
            console.log(course)
            console.log(new_course)

        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(new_course))
        res.end('Add course to our resources')

    } else if (req.url == '/updateCourse' && req.method == 'PUT'){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end('Update course to our resources')
    } else if (req.url == '/archiveCourse' && req.method == 'DELETE'){
        res.writeHead(200, {'Content-Type': 'text/plain'})
        res.end('Archive course to our resources')
    } else {
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end('Page not available')
    }
})

server.listen(port)

console.log(`Server is now accessible at localhost ${port}`)