const http = require('http');

const port = 4000

const server = http.createServer(function (request, response){
    // The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
    if ( request.url == '/items' && request.method == 'GET') { // In this case, we are checking if the current request method is a 'GET HTTP' method
        response.writeHead(200, {'Content-Type': 'text/plain'});
    // Ends the response process
        response.end('Data is retrieved from the Data Base')
    } else if (request.url == '/items' && request.method == 'POST') { // In this case, we are checking if the current request method is a 'POST HTTP' method
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Data is sent to the Data Base')
    }
    //  else if (request.url == '/items' && request.method == 'PUT') {
    //     response.writeHead(200, {'Content-Type': 'text/plain'});
    //     response.end('Data is updated from the Data Base')
    // } else if (request.url == '/items' && request.method == 'DELETE') {
    //     response.writeHead(200, {'Content-Type': 'text/plain'});
    //     response.end('Data is deleted from the Data Base')
    // }
})

server.listen(port)

console.log(`Server is now accessible at localhost ${port}`)